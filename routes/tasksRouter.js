var router = require('express').Router();
var tasksController = require('../controllers/tasksController');

router.route('/')
    .get(tasksController.index);
router.route('/task/show/:id')
    .get(tasksController.show);
router.route('/task/add/')
    .get(tasksController.add);
router.route('/task/del/')
    .get(tasksController.del);

module.exports = router;