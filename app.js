global.config = require('./config.json');
global.db = require('./db.js');
var express = require('express');
var tasksRouter = require('./routes/tasksRouter');

var app = express();
app.use(express.static('public'));
app.use('/', tasksRouter);

app.listen(config.server.port, function(){
    console.log('listen on localhost:' + config.server.port);
});