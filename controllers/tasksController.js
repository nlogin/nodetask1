var tasks = global.db.tasks;

exports.index = function(req, res, next) {
    var list = '<ul>';
    tasks.forEach(function(item, i){
        list += '<li>' +
            '<a href="/task/show/'+i+'"' + '>' + item.title + '</a>' +
            '<a href="/task/del?id='+i+'"' + '>|X|</a>' +
            '</li>';
    });
    list += '</ul>';
    list += '<form action="/task/add" method="get">' +
        '<label>Заголовок</label><input name="title" type="text"/><br>' +
        '<label>Текст</label><textarea name="text"></textarea>' +
        '<input type="submit" value="Добавить"/>' +
        '</form>';
    res.status(200).send(list);
    next();
};

exports.show = function(req, res, next) {
    if (req.params.id >= 0 && tasks[req.params.id] != undefined) {
        res.status(200).send(tasks[req.params.id]);
    } else {
        res.status(404).send('Not Found');
    }

    next();
};

exports.add = function(req, res, next) {
    var title = req.query.title;
    var text = req.query.text;
    if (text == undefined || text == '' || title == undefined || title == '') {
        res.status(200).send('Empty');
    } else {
        tasks.push({title: title, text: text});
        res.redirect('/');
    }

    next();
};

exports.del = function(req, res, next) {
    var id = req.query.id;
    if (id == undefined || id == '') {
        res.status(200).send('Empty');
    } else {
        delete tasks[id];
        res.redirect('/');
    }

    next();
};